# postcss-remove-bem-blocks

[PostCSS](https://github.com/postcss/postcss)-плагин для удаления БЭМ-блоков (включая их модификаторы и элементы) из конечного CSS.

## Установка

```bash
yarn add -D @advdominion/postcss-remove-bem-blocks
```

## Использование

`postcss.config.js`

```js
import removeBemBlocks from '@advdominion/postcss-remove-bem-blocks';

const plugins = [];

if (process.env.NODE_ENV === 'production') {
    plugins.push(
        removeBemBlocks({
            blocks: ['.stylepage'],
        })
    );
}

export default {
    plugins,
};
```
