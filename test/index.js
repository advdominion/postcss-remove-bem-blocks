import {readFile, writeFile} from 'node:fs/promises';
import postcss from 'postcss';
import plugin from './../index.js';

readFile('from.css', {
    encoding: 'utf-8',
})
    .then((css) => {
        return postcss([
            plugin({
                blocks: ['.block-2'],
            }),
        ]).process(css, {
            from: 'from.css',
        });
    })
    .then(({css}) => {
        writeFile('to.css', css, {
            encoding: 'utf-8',
        });
    });
